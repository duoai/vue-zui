var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractPlugin = require('extract-text-webpack-plugin');
var config = require('./webpack.base.config');

var vPath="./src/components/";

config.entry={
   "vui":vPath+"vui"
}
config.output={
    path: path.join(__dirname, './dist'),
    // 文件地址，使用绝对路径形式
    filename: '[name].min.js',
    //[name]这里是webpack提供的根据路口文件自动生成的名字
    publicPath: './',
    // 公共文件生成的地址
    library: 'vui',
    libraryTarget: 'umd'
};
config.devtool="";
config.plugins = (config.plugins || []).concat([
  // this allows uglify to strip all warnings
  // from Vue.js source code.
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: '"production"'
    }
  }),
  // This minifies not only JavaScript, but also
  // the templates (with html-minifier) and CSS (with cssnano)!
  new webpack.optimize.UglifyJsPlugin({
    compress: {
      warnings: false
    }
  }),
  new webpack.optimize.OccurenceOrderPlugin()
])

module.exports = config