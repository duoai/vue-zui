import tab from "./tab";
import tabItem from "./tabItem";
import tabPanel from "./tabPanel";

export {
	tab,tabItem,tabPanel
}